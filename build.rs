fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Use tonic build to generate the stubs and definitions from our proto files
    tonic_build::compile_protos("proto/payments.proto")?;
    Ok(())
}
