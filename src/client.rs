use payments::bitcoin_client::BitcoinClient;
use payments::BtcPaymentRequest;

// This module is used to include the types that tonic generates from our proto file
// these types are brought into scope on lines 1 and 2.
pub mod payments {
    tonic::include_proto!("payments"); // The package name is used here.
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = BitcoinClient::connect("http://[::1]:50051").await?;

    let request = tonic::Request::new(BtcPaymentRequest {
        from_addr: "123456".to_owned(),
        to_addr: "987654".to_owned(),
        amount: 55,
    });

    let response = client.send_payment(request).await?;

    println!("RESPONSE: {:?}", response);

    Ok(())
}
